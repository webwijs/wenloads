<?php

namespace Webwijs\FacetSearch\Filter;

interface PaginateInterface
{
	public function getCurrentPage();

    public function getNumPages();

    /**
     * Returns the pagination url for the specified page number.
     *
     * @param int $paged the page number
     * @return string the pagination url for the specified page number.
     */
    public function getPaginationUrl($paged);
}