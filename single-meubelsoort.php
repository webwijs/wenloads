<?php
/*
 * Layouts: meubelsoort
 */
 the_post();
	echo $this->partial('partials/parts/extra-title.phtml'); // optional title meta box
	echo $this->partial('partials/parts/subnav.phtml'); // optional subnavigation widget
?>
<section class="row singular template-<?php echo get_post_type() ?>" >
	<div class="large-6 small-12 columns">
		<article class="post-content" itemscope itemtype="http://schema.org/Article">
			<?php echo $this->partial('partials/page/singular.phtml') ?>
		</article>
	</div>
	<div class="large-6 small-12 columns">
        <?php echo $this->sidebarArea('col-right') ?>
	</div>
</section>

<!-- Gallery stuffs -->

<section class="bottom">
	<div class="row align-center">
		<div class="large-6 column">
			<?php echo $this->sidebarArea('bottom') ?>
		</div>
	</div>
	<div class="row align-center">
		<div class="large-12 column">
			<div class="backbtn">
        		<a class="readmore back" href="<?php echo get_permalink(get_option( 'theme_page_meubelsoorten' )); ?>"><?php echo __('Terug naar overzicht') ?></a>
        	</div>
		</div>
	</div>
</section>

<?php
	echo $this->partial('partials/parts/page-bottom.phtml'); // breadcrumbs and social buttons
?>
