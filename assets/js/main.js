jQuery(document).ready(function($) {

    $(window).on("scroll", function(event) {
        if ($('body').width() > 625) {

            var fromTop = $(document).scrollTop();
            $('header').toggleClass("scrolled", (fromTop > 20));
            var scrolled = false;
            // if(fromTop > 150){
            //     if($('.stickynav #searchform').length == 0){
            //          var element = $('#searchform').detach();
            //         $('.stickynav').append(element);
            //     }
            // }
            // else{
            //     if($('.normalnav #searchform').length == 0){
            //         var element = $('#searchform').detach();
            //         $('.normalnav').append(element);
            //     }
            // }

        }
    });

    $(document).on('click', '.toggle', function(e) {
        e.preventDefault();
        var self = $(this);
        var target = $(self.attr('href'));
        target.toggleClass('active');
        self.toggleClass('activated');
        return false;
    });

    $(document).on('click', '.scrollto', function(e) {
        e.preventDefault();
        var self = $(this);
        var target = $(self.data('target'));
        $('html,body').animate({
           scrollTop: target.offset().top
        });
        return false;
    });

    $('.content-header').slick({
        'arrows': false,
        'dots': true,
        'autoplay':true,
        'autoplaySpeed':4000,
        'speed': 1500,
    });
	$('#page-slider').slick({
		arrows: true,
		prevArrow: '<div class="prev-arrow"><i class="fa fa-arrow-left"/></div>',
		nextArrow: '<div class="next-arrow"><i class="fa fa-arrow-right"/></div>',
		dots: false,
		autoplay:false,
		autoplaySpeed:4000,
		speed: 1500,
		slidesToShow: 1,
		slidesToScroll: 1,
		centerMode: true,
		responsive: [
			{
				breakpoint: 4000,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 1,
					centerMode: false
				}
			},
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: false
				}
			}
		]
	});

    $('.headerslider').slick({
        'arrows': false,
        'dots': true,
        'autoplay':true,
        'autoplaySpeed':4000,
        'speed': 1500,
    });


});
