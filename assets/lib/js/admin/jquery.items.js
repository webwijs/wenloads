jQuery(document).ready(function($) {
    var $container = $('.sortable-items-container');
    
    $.each($container, function(key, element) {
        var $list   = $(this).find('ol');
        var $button = $(this).find('.button');
        var $value = $(this).find('.value');


        $list.sortable({
            placeholder: 'sortable-placeholder'
        });
        
        $(this).find('.button').click(function() {
            
            $button = $(this);
            var data = {
                action: 'load_items',
                type: $button.data('name'),
                content: encodeURIComponent($value.val()),
            }
            
            $.post(ajaxurl, data, function(response) {
                console.log(response);     
                $list.append(response);
            });
        });
    });
    
    /**
     * Removes the tab by removing the list item which contains the tab.
     */
    $container.on('click', '.item-delete', function(event) {
        $(this).closest('li').remove();
        
        // prevent default action.
        event.preventDefault();
    }); 
});