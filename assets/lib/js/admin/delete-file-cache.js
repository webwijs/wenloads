jQuery( document ).ready(function($) {
    var deleteButton = $('#delete-file-cache');

    deleteButton.click(function() {
        $.ajax({
          type: "POST",
          url: ajaxurl,
          data: {
              action: 'delete_cache'
          },
          success: function () {
              var msg = '<span class="dashicons dashicons-trash"></span>&nbsp;&nbsp; cache verwijderd';
              var duration = 1500;

              var el = document.createElement("div");
              el.setAttribute("style","position:absolute;top:50px;left:calc(50% - 125px);width:250px;text-align:center;font-size:18px;padding:12px 0;border-radius:5px;background-color:#FFF;");
              el.innerHTML = msg;
              setTimeout(function(){
               el.parentNode.removeChild(el);
              },duration);
              document.body.appendChild(el);
            console.log('Cache removed!');
         }
        });
    });
});
