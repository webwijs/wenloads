<?php
/*
 * Layouts: page
 */
 the_post();
	echo $this->partial('partials/parts/extra-title.phtml'); // optional title meta box
?>
<section class="row singular template-<?php echo get_post_type() ?>" >
	<div class="small-12 columns">
		<article class="post-content" itemscope itemtype="http://schema.org/Article">
			<?php echo $this->partial('partials/page/singular.phtml') ?>
		</article>
	</div>
</section>
<?php
	echo $this->partial('partials/parts/page-bottom.phtml'); // breadcrumbs and social buttons
?>
