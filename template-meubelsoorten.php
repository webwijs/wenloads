<?php
/**
 * Template Name: Meubelsoorten
 * Layouts: one-column
 */
?>
<?php the_post(); ?>

<section class="meubels">
    <div class="row align-center">
        <div class="small-12 column">
            <?php echo $this->partial('partials/page/singular.phtml') ?>
        </div>
    </div>
    <div class="row align-left">
        <?php echo $this->listChildren('meubelsoort'); ?>
    </div>
</section>

<?php
    echo $this->partial('partials/parts/page-bottom.phtml'); // breadcrumbs and social buttons
?>