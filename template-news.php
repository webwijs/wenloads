<?php
/**
 * Template Name: News
 * Layouts: news
 */
 $this->facetedSearch()->init(array(
	'type' => 'News',
	'template' => 'partials/news/faceted-search.phtml',
	'vars' => array(
		'list-item' => 'partials/news/list-item.phtml',
		'not-found' => 'partials/news/not-found.phtml'
	)
));

 the_post();
 	echo $this->partial('partials/parts/extra-title.phtml'); // optional title meta box
 //	echo $this->partial('partials/parts/subnav.phtml'); // optional subnavigation widget

 ?>

<section class="row singular template-<?php echo get_post_type() ?>" >
	<div class="small-12 columns">
		<article class="post-content" itemscope itemtype="http://schema.org/Article">
			<?php echo $this->facetedSearch(); ?>
		</article>
	</div>
</section>
<?php echo $this->partial('partials/parts/page-bottom.phtml'); // breadcrumbs and social buttons
?>
