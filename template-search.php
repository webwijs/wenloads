<?php
/*
 * Template Name: Zoekresultaten
 * Layouts: page
 */
?>
<?php the_post();
echo $this->partial('partials/parts/extra-title.phtml'); // optional title meta box
	echo $this->partial('partials/parts/subnav.phtml'); // optional subnavigation widget
?>
<section class="row singular template-<?php echo get_post_type() ?>" >
	<div class="large-12 small-12 columns">
		<article class="post-content" itemscope itemtype="http://schema.org/Article">
			<?php echo $this->partial('partials/search/singular.phtml') ?>
		</article>
	</div>
</section>
<?php
	echo $this->partial('partials/parts/page-bottom.phtml'); // breadcrumbs and social buttons
?>
