<?php

namespace Theme\Admin;

use Webwijs\AbstractBootstrap;
use Webwijs\Admin\Metabox\PageLayout;
use Webwijs\Admin\Metabox\Excerpt;
use Webwijs\Admin\Metabox\Multibox;
use Theme\Admin\Controller\SettingsController;
use Theme\Admin\Controller\DeveloperSettingsController;
use Theme\Admin\Controller\SidebarsController;

use Theme\Admin\Metabox\Meubel;

class Bootstrap extends AbstractBootstrap
{
    protected function _initSettings()
    {
        add_action('admin_head', function(){
          wp_enqueue_style('bootstrap', get_template_directory_uri(). '/assets/lib/css/bootstrap.css');
        });
		// Normal menu
        $builder = SettingsController::getBuilder();
        $builder->group('page', __('Pagina koppelingen'))
				->add('theme_page_404', 'postSelect', array('label' => __('404 pagina')))
				->add('theme_page_contact', 'postSelect', array('label' => __('Contactpagina')))
                ->add('theme_page_meubelsoorten', 'postSelect', array('label' => __('Meubelsoorten pagina')));

		$builder->group('layout', __('Footer blokken'))
				->add('theme_layout_footer_opening', 'contentBlockSelect', array('label' => __('Openingstijden blok')))
				->add('theme_layout_footer_other', 'contentBlockSelect', array('label' => __('Kijk ook eens bij-blok')));

		$builder->group('images', __('Standaardafbeeldingen'))
				->add('theme_image_news_thumb', 'imageSelect', array('label' => __('Standaard uitgelichte afbeelding nieuwsberichten')))
				->add('theme_image_news_header', 'imageSelect', array('label' => __('Standaard header afbeelding nieuwsberichten')));

		$builder->group('form', 'Formulieren')
                ->add('theme_form_newsletter', 'formSelect', array('label' => __('Nieuwsbriefformulier')));
		$builder->group('socialmedia', 'Social media')
				->add('theme_social_instagram', 'text', array('label' => __('Instagram pagina')));

        $builder->group('company', __('Contactgegevens'))
                ->add('theme_company_email', 'text', array('label' => __('E-mailadres')))
                ->add('theme_company_phone', 'text', array('label' => __('Telefoonnummer (algemeen)')))
                ->add('theme_company_helpdesk', 'text', array('label' => __('Telefoonnummer (helpdesk)')))
                ->add('theme_company_commerce_number', 'text', array('label' => __('KVK-nummer')))
                ->add('theme_company_tax_number', 'text', array('label' => __('BTW-nummer')))
                ->add('theme_company_iban_number', 'text', array('label' => __('IBAN')))
                ->add('theme_company_address', 'text', array('type' => 'textarea', 'label' => __('Vestigingsplaats')));

        new SettingsController();
    }

    protected function _initSidebars()
    {
        $sidebarsController = new SidebarsController();
    }

	protected function _initDevelopersMenu() {
		// Developer menu
		$developer = DeveloperSettingsController::getBuilder();
		$developer->group('hide', 'Verberg elementen');
		$developer->group('advanced', __('Geavanceerd'))
				->add('theme_advanced_flat_url', 'checkbox', array('label' => __('Platte URL\'s')))
				->add('theme_advanced_scss_compiler', 'compilerSelect', array('label' => __('SCSS compiler modus')))
				->add('theme_advanced_compile_scss_now', 'compile', array('label' => __('Compileer SCSS nu')));

		$developer->group('cache', __('Cache'))
				->add('theme_advanced_varnish', 'checkbox', array('label' => __('Varnish inschakelen')))
				->add('theme_advanced_redis_cache', 'checkbox', array('label' => __('Redis Cache inschakelen')))
				->add('theme_advanced_delete_file_cache', 'deleteFileCache', array('label' => __('Verwijder bestandsysteem cache')));
		new DeveloperSettingsController();

	}

    protected function _initRoles()
    {
        add_filter('editable_roles', array('Theme\Admin\Filter\User', 'editableRoles'));
    }

    protected function _initEditor()
    {
        //add_filter('mce_external_plugins', array('Theme\Admin\Filter\TinyMCE', 'addButtonPlugin'));
        //add_filter('mce_buttons_2', array('Theme\Admin\Filter\TinyMCE', 'registerButtons'));

        add_filter('mce_buttons_2', array('Theme\Admin\Filter\TinyMCE', 'styleSelect'));
        add_filter('tiny_mce_before_init', array('Theme\Admin\Filter\TinyMCE', 'stylesDropdown'));

        add_action('admin_init', array('Theme\Admin\Filter\TinyMCE', 'enqueueStyles'));
        add_action('admin_head', array('Theme\Admin\Filter\TinyMCE', 'editorStyle'));
        add_filter('tiny_mce_before_init', array('Theme\Admin\Filter\TinyMCE', 'editorInit'));
    }

    protected function _initAdminLayout()
    {
        add_action('admin_head', array('Theme\Admin\Action\AdminLayout', 'menuLayout'));
        add_action('admin_menu', array('Theme\Admin\Action\AdminLayout', 'menuItems'));
        add_action('wp_dashboard_setup', array('Theme\Admin\Action\AdminLayout', 'removeWidgets'));
        add_action('add_meta_boxes', array('Theme\Admin\Action\AdminLayout', 'removeMetaBoxes'));
        add_action('add_meta_boxes', array('Theme\Admin\Action\YoastSeo', 'lessIntrusive'));
        add_action('add_meta_boxes', array('Theme\Admin\Action\AdminLayout', 'removeMetaBoxes'));
        add_filter('manage_posts_columns', array('Theme\Admin\Action\AdminLayout', 'listTableColumns'), 10, 2);
        add_filter('manage_pages_columns', array('Theme\Admin\Action\AdminLayout', 'listTableColumns'), 10, 2);
        foreach (get_post_types() as $post_type) {
            add_filter('get_user_option_closedpostboxes_' . $post_type, array('Theme\Admin\Action\AdminLayout', 'closedMetaBoxes'));
            add_filter('get_user_option_metaboxhidden_' . $post_type, array('Theme\Admin\Action\AdminLayout', 'hiddenMetaBoxes'));
        }
    }

    protected function _initPages()
    {
        Multibox::register('page', array('id' => 'settings', 'title' => 'Instellingen', 'boxes' => array(
            array('class' => 'Webwijs\Admin\Metabox\Visibility'),
            array('class' => 'Webwijs\Admin\Metabox\ParentPage'),
            array('class' => 'Webwijs\Admin\Metabox\MenuOrder')
        )));

        PageLayout::register('page');
        Excerpt::register('page');

        Meubel::register('page');

        Multibox::register('page', array('id' => 'texts', 'context'=>'normal', 'title' => 'Extra teksten', 'boxes' => array(
            array('class' => 'Webwijs\Admin\Metabox\Text', 'settings' => array('id' => 'text_extratitle', 'title' => 'Extra titel')),
            // array('class' => 'Webwijs\Admin\Metabox\Text', 'settings' => array('id' => 'text_videotitle', 'title' => 'Video titel')),
            // array('class' => 'Webwijs\Admin\Metabox\Text', 'settings' => array('id' => 'text_videosubtitle', 'title' => 'Video subtitel'))
        )));
    }

    protected function _initMeubelsoort(){
        PageLayout::register('meubelsoort');
    }
}
