<?php

namespace Theme\Helper;


class WcGetCategories
{

	public function wcGetCategories(){
	  
	  	$template = 'partials/woocommerce/cat-list.phtml';
		// $args = array(
		// 	'number'     => $number,
		// 	'orderby'    => $orderby,
		// 	'order'      => $order,
		// 	'hide_empty' => $hide_empty,
		// 	'include'    => $ids
		// );

		// $product_categories = get_terms( 'product_cat', $args );

		// since wordpress 4.5.0
		$args = array(
			'taxonomy'   => "product_cat",
			'number'     => 5,
			// 'orderby'    => 'rand',
			// 'order'      => 'ASC',
			'hide_empty' => false
		);
		$product_categories = get_terms($args);

		$output = '';
        if ($product_categories) {
            $output = $this->view->partial($template, array('categories' => $product_categories));
        }
        
        return $output;

	}

}