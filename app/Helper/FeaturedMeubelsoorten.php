<?php

namespace Theme\Helper;

use Webwijs\Post;

class FeaturedMeubelsoorten
{
    public function featuredMeubelsoorten($args = null, $post = null)
    {
        if ($post === null) {
            $post = $GLOBALS['post'];
        }
        
        $defaults = array(
            'relation_key' => 'selected_meubelsoort',
            'template'     => 'partials/woocommerce/cat-list.phtml',
            'vars'         => null,
        );
        $args = array_merge($defaults, (array) $args);
        
        $posts = array();
        
        $postIds = Post::getRelatedPostIds($args['relation_key'], $post);
        if (!empty($postIds)) {
            $posts = get_posts(array(
                'post__in'  => $postIds,
                'post_type' => array('meubelsoort'),
                'orderby'   => 'post__in',
                'order'     => 'asc',
            ));
        }
        
        return $this->view->partial($args['template'], array_merge((array) $args['vars'], array('categories' => $posts)));
    }
}