<?php

namespace Theme\Helper;

use Webwijs\Post;
use Webwijs\Util\Arrays;

class ListChildren
{
    /**
     * Returns a partial that displays all the children of the given post,
     * or if not provided the current post.
     *
     * @param string $postType the post type for which to retrieve children.
     * @param array|null $args optional arguments to change the output.
     * @param object|null $post the post object for which to display children.
     * @return string a partial displaying all the children of the given post.
     */
    public function listChildren($postType = 'page', $args = null, $post = null)
    {    
        if (!is_object($post)) {
            $post = $GLOBALS['post'];
        }
        
        $defaults = array(
            'queryArgs' => array(
                'post_type' => $postType,
                'nopaging' => true,
                'orderby' => 'menu_order',
                'order' => 'asc'
            ),
            'template' => 'partials/' . $postType . '/list.phtml',
            'vars' => array(
                'show_excerpt' => false,
                'currentPageId' => get_the_ID()
            )
            
        );
        $args = Arrays::addAll($defaults, (array) $args);

        // remove 'post_parent' if a custom page is set for the given post type.
        if (!is_null(Post::getCustomPostPageId($postType))) {
            unset($args['queryArgs']['post_parent']);
        }

        $output = '';
        query_posts($args['queryArgs']);
        if (have_posts()) {
            $output = $this->view->partial($args['template'], $args['vars']);
        }
        wp_reset_query();
        
        return $output;
    }
}
