<?php

namespace Theme\Helper;


class WcShowAttributes
{

    public function wcShowAttributes(){
      
        // Edit below with the title of the attribute you wish to display
        $desired_atts = array( 'ISBN','Formaat','Pagina\'s','Auteur' );

     
        // sanitize attributes into taxonomy slugs
        foreach ( $desired_atts as $att ) {
          $tax_slugs[] = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '_', $att)));
        }
         
        global $product;
        $attributes = $product->get_attributes();

         
        if ( ! $attributes ) {
            return;
        }
          
        $attr = [];
        foreach ( $attributes as $attribute ) {
            $name = $attribute->get_name();

              
            if ( $attribute->is_taxonomy() ) {
              
                $clean_name = $attribute['name'];
                // Trim pa_ prefix
                if ( 0 === strpos( $clean_name, 'pa_' ) ) {
                  $clean_name = substr( $clean_name, 3 );
                }
                 
                // if this is a desired att, get value and label
                if ( in_array( $clean_name,  $tax_slugs ) ) {

                    $terms = wp_get_post_terms( $product->get_id(), $name, 'all' );
                    // get the taxonomy
                    $tax = $terms[0]->taxonomy;
                    // get the tax object
                    $tax_object = get_taxonomy( $tax );
                    // get tax label
                    if ( isset ( $tax_object->labels->singular_name ) ) {
                        $tax_label = $tax_object->labels->singular_name;
                    } elseif ( isset( $tax_object->label ) ) {
                        $tax_label = $tax_object->label;
                        // Trim label prefix since WC 3.0
                        if ( 0 === strpos( $tax_label, 'Product ' ) ) {
                           $tax_label = substr( $tax_label, 8 );
                        }
                    }
     
                    $attr[$clean_name]['name'] = $tax_label;
                    $tax_terms = array();
                    foreach ( $terms as $term ) {
                        $single_term = esc_html( $term->name );
                        array_push( $tax_terms, $single_term );
                    }
                    $attr[$clean_name]['terms'] = $tax_terms;

                  
                } // our desired att
                  
            } else {
              
                // for atts which are NOT registered as taxonomies
                  // var_dump($name);
                // if this is desired att, get value and label
                if ( in_array( $name, $desired_atts ) ) {
                    $attr[$name]['name'] = $name;
                    $attr[$name]['terms']= $attribute->get_options();
                }
            }
          
        }
        $out = $this->view->partial('partials/woocommerce/attributes.phtml', array('attributes' => $attr));
        echo $out;
    }

}