<?php

namespace Theme\Helper;


class HasKids
{

    public function hasKids(){
      
        global $post;

	    $children = get_pages( array( 'child_of' => $post->ID ) );
	    $siblings = get_pages( array( 'child_of' => $post->post_parent ) );

	    if( (count($children) > 0) || (count($siblings) > 1) ) {
	        return true;
	    } else {
	        return false;
	    }

    }

}