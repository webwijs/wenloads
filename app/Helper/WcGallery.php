<?php

namespace Theme\Helper;


class WcGallery
{

    public function wcGallery(){
      
        global $post, $product;

        $attachment_ids = $product->get_gallery_image_ids();
        $out = $this->view->partial('partials/woocommerce/gallery.phtml', array('attachment_ids' => $attachment_ids));
        echo $out;
    }

}