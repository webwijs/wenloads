<?php

namespace Theme\Cpt;

use Webwijs\Cpt;

class Meubelsoorten extends Cpt
{
    public $type = 'meubelsoort';
    
    public function init()
    {
        $this->labels = array(
            'name' => __('Meubelsoorten', 'theme'),
            'singular_name' => __('Meubelsoorten', 'theme'),
            'add_new' => __('Nieuwe meubelsoort', 'theme'),
            'add_new_item' => __('Nieuwe meubelsoort', 'theme'),
            'edit_item' => __('Meubelsoort bewerken', 'theme'),
            'new_item' => __('Nieuwe meubelsoort', 'theme'),
            'view_item' => __('Meubelsoorten bekijken', 'theme'),
            'search_items' => __('Meubelsoort zoeken', 'theme'),
            'not_found' => __('Geen meubelsoorten gevonden', 'theme'),
            'not_found_in_trash' => __('Geen meubelsoorten gevonden', 'theme'),
            'menu_name' => __('Meubelsoorten', 'theme')
        );
        
        $this->settings = array(
            'rewrite' => false,
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'supports' => array('title', 'excerpt', 'editor', 'thumbnail')
        );
    }
    
    public static function register($options = null)
    {
        $cpt = new self($options);
    }
    
    public static function queryPosts($args = null)
    {
        $defaults = array(
            'post_type' => $this->type,
            'orderby' => 'menu_order',
            'order' => 'ASC'
        );
        $args = array_merge($defaults, (array) $args);
        return query_posts($args);   
    }
}
