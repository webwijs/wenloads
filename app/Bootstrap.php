<?php
namespace Theme;

use Webwijs\Application;
use Webwijs\Post;
use Webwijs\Loader\AutoloaderFactory;
use Webwijs\Loader\ClassLoader;
use Webwijs\Shortcode\ViewHelper;
use Webwijs\AbstractBootstrap;
use Webwijs\CSS\MainCompiler as CSSCompiler;

use Theme\Cpt\Meubelsoorten as MeubelsoortenCpt;

require_once dirname(__DIR__) . '/lib/php/Webwijs/AbstractBootstrap.php';
require_once dirname(__DIR__) . '/lib/php/Webwijs/Application.php';

class Bootstrap extends AbstractBootstrap
{
    public function init()
    {
        $application = new Application();
        $application->init();
        parent::init();
    }

    protected function _initAutoloaderConfig()
    {
        AutoloaderFactory::factory(array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Theme' => __DIR__,
                ),
            ),
        ));
    }

    /**
     * Add static resources to the class loader.
     */
    protected function _initResourceloaders()
    {
        ClassLoader::addStaticResources(array(
            'viewhelper'  => 'Theme\Helper',
            'formelement' => 'Theme\Admin\Controller\Form\Element',
        ));
    }

    protected function _initModels()
    {
        Application::getModelManager()
            ->addModel('Sidebar', 'Webwijs\Model\Sidebar');
    }

    protected function _initLayouts()
    {
        Application::getServiceManager()->get('PageLayout')
			->addLayout('news', array(
				'name' => 'Nieuws',
				'icon' => 'assets/lib/images/layouts/two-columns-right.png',
				'sidebars' => array(),
			))
			->addLayout('maatwerk', array(
				'name' => 'Maatwerk',
				'icon' => 'assets/lib/images/layouts/one-column.png',
				'sidebars' => ['col-right' => 'Pagina: rechter sidebar', 'subnav' => 'Pagina: subnavigatie', 'content-slider' => 'Slider', 'content-usps' => 'USPs']
			))
            ->addLayout('home', array(
                'name' => 'Home',
                'icon' => 'assets/lib/images/layouts/one-column.png',
                'sidebars' => array('content-usps' => 'USPs','content-featured' => 'Uitgelicht'),
            ))
            ->addLayout('meubelsoort', [
                'name' => 'Meubelsoort',
                'icon' => 'assets/lib/images/layouts/one-column.png',
                'sidebars' => ['col-right' => 'Pagina: rechter sidebar', 'bottom' => 'Pagina: onderin']
            ])
			->addLayout('contact', [
				'name' => 'Pagina',
				'icon' => 'assets/lib/images/layouts/one-column.png',
				'sidebars' => ['contact-left' => 'Contact: linker kolom', 'subnav' => 'Pagina: subnavigatie', 'visitus' => 'Contact: bezoek ons tekst']
			])
			->addLayout('page', [
				'name' => 'Pagina',
				'icon' => 'assets/lib/images/layouts/one-column.png',
				'sidebars' => ['col-right' => 'Pagina: rechter sidebar', 'subnav' => 'Pagina: subnavigatie']
			])
            ->setDefaultLayout('page');

        $options = array(
            'before_widget' => '<div class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        );
		// $options = array(
        //     'before_widget' => '',
        //     'after_widget' => '',
        //     'before_title' => '',
        //     'after_title' => '',
        // );

        $sidebars = Application::getModelManager()->getTable('Sidebar')->findAll();
        if(is_array($sidebars)) {
            foreach ($sidebars as $sidebar) {
                register_sidebar(array_merge($options, array('name' => $sidebar->name, 'id' => $sidebar->code)));
            }
        }
        add_filter('theme_default_sidebar', array('Theme\Filter\PageLayout', 'getDefaultSidebar'), 10, 3);
    }

    protected function _initMenus()
    {
        register_nav_menu('main', 'Hoofdmenu');
        register_nav_menu('service', 'Servicemenu');
        register_nav_menu('footer', 'Footermenu');

        add_filter('wp_nav_menu_objects', array('Theme\Filter\Menu', 'itemAncestors'));
        add_filter('show_admin_bar', '__return_false');
        add_filter('sitemap_post_types', array('Theme\Filter\Sitemap', 'PostTypes'));
    }

    protected function _initCpts()
    {
        Post::addCustomPostPageId('meubelsoort',get_option('theme_page_meubelsoorten'));
        MeubelsoortenCpt::register();
    }

    protected function _initFilters()
    {
        add_filter('slideshow_content', function($content) {
            $lines = preg_split( '/\r\n|\r|\n/', $content);
            if(is_array($lines) && !empty($lines)) {
                $content = '';
                foreach($lines as $index => $line) {
                    $content .= sprintf('<span class="t%s">%s</span>', $index, $line);
                }
            }

            if($priority = has_filter('the_content', 'wpautop')) {
                remove_filter('the_content', 'wpautop');
                $content = apply_filters('the_content', $content);
                add_filter('the_content', 'wpautop', $priority);
            }
            return $content;
        });

        add_filter('upload_mimes', function($mimes) {
            $mimes['svg'] = 'image/svg+xml';
            $mimes['svgz'] = 'image/svg+xml';
            return $mimes;
        });

    }

    protected function _initWidgets()
    {
        remove_action('init', 'wp_widgets_init', 1);
        add_action('init', array('Theme\Action\Widgets', 'init'));

        register_widget('Webwijs\Widget\SectionNav');

        add_action('widgets_init', function() {
            unregister_widget('WP_Widget_Recent_Posts');
        });
    }
	protected function _initPageNotFoundTitle(){
		// Hook into wp_title filter hook
		add_filter( 'wp_title', function($title){
			if ( is_404() ) {
				$title = 'Pagina niet gevonden - '.get_bloginfo( 'name' ) ;
			}
			return $title;
		}, 18, 1);

	}
    protected function _initCSSCompiler()
    {
        if (defined('WP_DEBUG') && true === WP_DEBUG) {
            self::compileSCSS(true);
        } else {
            $compilerMode = get_option('theme_advanced_scss_compiler');
            switch ($compilerMode) {
                case '':
                    self::compileSCSS();
                    return true;
                    break;
                case 'forced':
                    self::compileSCSS(true);
                    return true;
                    break;
                case 'disabled':
                    return false;
                    break;
                default:
                    self::compileSCSS();
                    return true;
            }
        }

    }

    public static function compileSCSS($force = false)
    {
            $cssCompiler = new CSSCompiler('SCSS');
            $cssCompiler->compile($force);
    }

    protected function _initAjax()
    {
        add_action('wp_ajax_compile_scss', array('Theme\Admin\Ajax', 'compile'));
        add_action('wp_ajax_delete_cache', array('Theme\Admin\Ajax', 'deleteFileCache'));
        add_action('wp_ajax_load_sortable_item', array('Theme\Action\Ajax', 'loadSortableItem'));
    }

    protected function _initMultipleThumbnails()
    {
        if (class_exists('MultiPostThumbnails')) {
            $types = array('meubelsoort', 'page');
            foreach($types as $type) {
                new \MultiPostThumbnails(array(
                        'label' => __('Header afbeelding 1', 'theme'),
                        'id' => 'header-image-1',
                        'post_type' => $type
                    )
                );
                new \MultiPostThumbnails(array(
                        'label' => __('Header afbeelding 2', 'theme'),
                        'id' => 'header-image-2',
                        'post_type' => $type
                    )
                );
                new \MultiPostThumbnails(array(
                        'label' => __('Header afbeelding 3', 'theme'),
                        'id' => 'header-image-3',
                        'post_type' => $type
                    )
                );
            }
			new \MultiPostThumbnails(array(
					'label' => __('Header afbeelding', 'theme'),
					'id' => 'overview-image',
					'post_type' => 'news'
				)
			);
        }
    }

    protected function _initImages()
    {
        add_theme_support('post-thumbnails');
        add_image_size('thumbnail', 168, 108, true);
		add_image_size('slider-thumbnail', 600, 440, true);
        add_image_size('homepage-header', 1920, 560, true);
        add_image_size('default-header', 1920, 262, true);
		add_image_size('news-article-header', 790, 420, true);
		add_image_size('news-overview', 375, 250, true);
        add_image_size('wc-cat-square', 500, 500, true);
        add_image_size('wc-cat-landscape', 500, 1014, true);
        add_image_size('wc-cat-portrait', 1050, 500, true);
        add_image_size('wc-cat-summary', 500, 600, true);

        add_image_size('featured-bg', 1000, 700, true);

        add_filter('post_thumbnail_html', array('Webwijs\Filter\Placeholder', 'getPlaceholder'), 10, 5);
    }

    protected function _initShortCodes()
    {
        $helper = new ViewHelper;
        add_shortcode('sitemap', array($helper, 'sitemap'));
        add_shortcode('button', array($helper, 'button'));

        add_filter('shortcode_html', array('Webwijs\Filter\HTML', 'shortcode'));
    }

    protected function _initContentWidth()
    {
        //important for the admin editor and resizing objects and images
        $GLOBALS['content_width'] = 460;
    }

    protected function _initSearch()
    {
        add_filter('posts_search', array('Theme\Filter\Search', 'filter'), 10, 2);
    }

    protected function _initXmlSitemap()
    {
        add_filter('option_sm_options', array('Theme\Filter\Sitemap', 'xmlSitemapExclude'));
    }

    protected function _initHeader()
    {
        remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
        remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
        remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
        remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
        remove_action( 'wp_head', 'index_rel_link' ); // index link
        remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
        remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
        remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
        remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
        remove_action( 'wp_head', 'wp_shortlink_wp_head' );
    }

	protected function _initWooCommerce(){
        $helper = new ViewHelper;
        if(function_exists('is_woocommerce')){
            add_action( 'after_setup_theme', function(){
                add_theme_support( 'woocommerce' );
            } );

            add_filter( 'woocommerce_show_page_title' , function() {
                return false;
            });

            add_filter('loop_shop_columns', function() {
                return 3; // 3 products per row
            });

            add_filter( 'rewrite_rules_array', function( $rules ) {
                $new_rules = array(
                    'onze-boeken/([^/]*?)/page/([0-9]{1,})/?$' => 'index.php?product_cat=$matches[1]&paged=$matches[2]',
                    'onze-boeken/([^/]*?)/?$' => 'index.php?product_cat=$matches[1]',
                    // 'onze-boeken/([^/]*?)/([^/]*?)/?$' => 'index.php?product_cat=$matches[2]'
                );
                return $new_rules + $rules;
            });
            add_filter( 'loop_shop_per_page', function( $cols ) {
                // $cols contains the current number of products per page based on the value stored on Options -> Reading
                // Return the number of products you wanna show per page.
                // $cols = 3;
                return $cols;
            }, 20 );

            add_filter( 'woocommerce_product_tabs', function($tabs) {
                unset($tabs['reviews']);
                unset($tabs['additional_information']);
                unset($tabs['description']);
                return $tabs;
            }, 98 );

            remove_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20 );
            remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
            remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
            remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
            remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

            add_action('woocommerce_single_product_summary', array($helper, 'wcShowAttributes'), 25);
            add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 26);
            add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 27);
        }
    }

    protected function _initForms()
    {
        add_action("gform_editor_js", array('Theme\Filter\GForms', 'editorScript'));
        add_filter('gform_field_standard_settings', array('Theme\Filter\GForms', 'fieldSettings'), 10, 2);
        add_filter('gform_form_settings', array('Theme\Filter\GForms', 'formSettings'), 10, 2);
        add_filter('gform_pre_form_settings_save', array('Theme\Filter\GForms', 'formSave'));
        add_filter('gform_tooltips', array('Theme\Filter\GForms', 'setTooltips'));
        add_filter('gform_submit_button', array('Theme\Filter\GForms', 'submitButton'), 10, 2);
        add_filter('gform_submit_button', array('Theme\Filter\GForms', 'addFooterDescription'), 100, 2);
        add_filter('gform_field_input', array('Theme\Filter\GForms', 'placeholder'), 10, 5);
        add_filter('gform_field_content', array('Theme\Filter\GForms', 'fieldContent'), 10, 5);
        add_filter('gform_field_css_class', array('Theme\Filter\GForms', 'fieldClasses'), 10, 3);
    }

    protected function _initMinHtml()
    {
        add_filter('theme_html_output', array('Webwijs\Filter\Minify', 'html'));
    }
	protected function _initTranslations()
    {
        $languageDir = dirname(__FILE__) . '/i18n';
        $locale = get_locale();
        if (file_exists($languageDir . '/gravityforms.' . $locale . '.mo')) {
            self::_loadTextDomain('gravityforms', $languageDir . '/gravityforms.' . $locale . '.mo');
        }
        add_action('init', function() {
            $locale = get_locale();
            $languageDir = dirname(__FILE__) . '/i18n';
            if ($locale != 'nl_NL') {
                Bootstrap::_loadTextDomain('default', $languageDir . '/theme.' . $locale . '.mo');
            }
        });
    }
    /* bypass load_textdomain to avoid filters */
    public static function _loadTextDomain($domain, $mofile)
    {
        global $l10n;
        $mo = new \MO();
        if (!file_exists($mofile) || !$mo->import_from_file($mofile)) {
            return false;
        }
        if (isset($l10n[$domain]) && !empty($l10n[$domain]->entries)) {
            $l10n[$domain]->merge_with($mo);
        }
        else {
            $l10n[$domain] = $mo;
        }
    }
}
