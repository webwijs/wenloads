<?php

namespace Theme\Widget;

use WP_Widget;

use Webwijs\View;

/**
 * Widget that displays the contents of a 'content block' post type.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class Product extends WP_Widget
{
    /**
     * Construct a new ContentBlock.
     */
    public function __construct()
    {
        $options = array('classname' => 'widget-product', 'description' => 'Toont een product voor in de Top 3');
        parent::__construct('widget_product_top', 'Product', $options);
    }

    /**
     * The form that is displayed in wp-admin and is used to save the settings for this widget.
     *
     * @param array $instance the form values stored in the database.
     */
    public function form($instance)
    {
        $defaults = array(
            'classname' => '',
            'product_id'  => '',
            'position'  => '',
        );
        $instance = array_merge($defaults, (array) $instance);
        $view = new View();
    ?>
        <p><label>Product<br />
            <?php echo $view->dropdownPosts(array(
                'post_types'       => 'product',
                'name'             => $this->get_field_name('product_id'),
                'class'            => 'widefat',
                'selected'         => $instance['product_id'],
                'show_option_none' => false,
            )) ?>
        </label></p>

        <p><label>Positie<br />
            <?php echo $view->dropdown($this->get_field_name('position'), array(
                'class'            => 'widefat',
                'selected'         => $instance['position'],
                'show_option_none' => false,
                'options'          => array('1'=>'1','2'=>'2','3'=>'3')
            )) ?>
        </label></p>

        <p><label>CSS-class voor container<br />
            <?php echo $view->formText($this->get_field_name('classname'), $instance['classname'], array('class' => 'widefat')) ?>
        </label></p>
    <?php
    }
    
    /**
     * Filter and normalize the form values before they are updated.
     *
     * @param array $new_instance the values entered in the form.
     * @param array $old_instance the previous form values stored in the database.
     * @return array the filtered form values that will replace the old values.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['product_id']  = (isset($new_instance['product_id']) && is_numeric($new_instance['product_id'])) ? $new_instance['product_id'] : 0;
        $instance['classname'] = (isset($new_instance['classname']) && is_string($new_instance['classname'])) ? $new_instance['classname'] : '';
        $instance['position'] = (isset($new_instance['position']) && is_string($new_instance['position'])) ? $new_instance['position'] : '';
        
        return $instance;
    }
    
    /**
     * Displays the widget using values retrieved from the database.
     *
     * @param array $args an array containing (generic) arguments for all widgets.
     * @param array $instance array the values stored in the database. 
     */
    public function widget($args, $instance)
    {
        $defaults = array(
            'product_id'   => '',
            'classname' => '',
            'position'  => '',
        );
        $instance = array_merge($defaults, $instance);
    
        $view = new View();
        if (is_string($instance['classname']) && strlen($instance['classname']) !== 0) {
            $args['before_widget'] = preg_replace('/class="/', 'class="' . $instance['classname'] . ' ', $args['before_widget'], 1);
        }

        if (($post = get_post($instance['product_id'])) !== null) {
            echo $args['before_widget'];
            echo $view->partial('partials/widgets/product.phtml', array_merge($args, $instance, array('post' => $post)));
            echo $args['after_widget'];
        }
    }
}
