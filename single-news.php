<?php
/*
 * Layouts: page
 */
 the_post();
	echo $this->partial('partials/news/extra-title.phtml'); // optional title meta box
	echo $this->partial('partials/news/date-bar.phtml'); // optional subnavigation widget
?>
<section class="row singular template-<?php echo get_post_type() ?>" >
	<div class="large-8 small-12 columns">
		<article class="post-content" itemscope itemtype="http://schema.org/Article">
			<figure class="entry-header">
			<?php
				$image = get_the_post_thumbnail_url(get_the_ID(), 'news-article-header');
				if(!empty($image)):
			?>
				<img src="<?php echo $image;?>"/>
			<?php endif;?>

			</figure>
			<?php echo $this->partial('partials/page/singular.phtml') ?>
		</article>
		<a href="<?php echo get_permalink(get_option('theme_page_news', true));?>" class="back-link"><span class="icon"><i class="ion ion-android-arrow-back"></i></span><span class="link-text"><?php echo __('Terug naar overzicht');?></span></a>
	</div>
	<div class="large-4 small-12 columns">
        <?php echo $this->sidebarArea('col-right') ?>
	</div>
</section>
<?php
	echo $this->partial('partials/parts/page-bottom.phtml'); // breadcrumbs and social buttons
?>
