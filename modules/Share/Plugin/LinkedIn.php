<?php

namespace Module\Share\Plugin;

use Theme\Admin\Controller\Form\FormBuilderInterface;

use Webwijs\Dom\HtmlElementBuilder;

/**
 * The LinkedIn plugin is a concrete implementation of the {@link PluginInterface} and allows a user
 * to share a web page using LinkedIn.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class LinkedIn implements PluginInterface
{
    /**
     * {@link inheritDoc}
     */
    public function isActive()
    {
        return (get_option('theme_share_linkedin') === '1');
    }

    /**
     * {@link inheritDoc}
     */
    public function getElement(array $args = array())
    {
        $defaults = array(
            'url' => ''
        );
        $args = array_merge($defaults, $args);

        // build anchor
        $builder = new HtmlElementBuilder('a');
        $builder->attributes(array(
                      'href'        => sprintf('http://www.linkedin.com/shareArticle?url=%1$s', urlencode($args['url'])),
                      'title'       => __('Deel deze pagina op LinkedIn'),
                      'class'       => 'popup linkedin',
                      'data-width'  => 990,
                      'data-height' => 450
                  ))
                ->child(function($builder) {
                        // build icon
                        return $builder->tag('i')
                                       ->attribute('class', 'fa fa-linkedin')
                                       ->build();
                  });

        return $builder->build();
    }

    /**
     * {@link inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->group('share', __('Delen'))
                ->add('theme_share_linkedin', 'checkbox', array('label' => __('Delen via LinkedIn')));
    }
}
