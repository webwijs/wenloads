<?php
namespace Module\Share;

use Module\Share\Plugin\Facebook;
use Module\Share\Plugin\LinkedIn;
use Module\Share\Plugin\Mail;
use Module\Share\Plugin\Twitter;

use Webwijs\AbstractBootstrap;
use Webwijs\Collection\ArrayList;
use Webwijs\Module\AutoloadableInterface;

/**
 * The Share bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap implements AutoloadableInterface
{
    /**
     * A collection of share plugins.
     * 
     * @var ListInterface
     */
    private static $plugins = null;

    /**
     * Register this module with the Standard PHP Library (SPL).
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\Share' => __DIR__,
                ),
            ),
        );
    }
    
    /**
     * Initialize the share types.
     */
    protected function _initShareTypes()
    {
        self::$plugins = new ArrayList(array(
            new Facebook(),
            new LinkedIn(),
            new Mail(),            
            new Twitter(),
        ));
    }
    
    /**
     * Register hooks for one or more WordPress actions.
     */
    protected function _initActions()
    {
        add_action('admin_menu', array('Module\Share\Action\Settings', 'addOptions')); 
    }
    
    /**
     * Returns a collection of share plugins.
     *  
     * @return ListInterface a collection of share plugins.
     */
    public static function getPlugins()
    {
        return self::$plugins;
    }
}
