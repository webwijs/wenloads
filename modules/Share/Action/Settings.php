<?php

namespace Module\Share\Action;

use Module\Share\Bootstrap;

use Theme\Admin\Controller\SettingsController;

/**
 * The Settings class contains actions which are related to the {@link SettingsController}.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class Settings
{
    /**
     * Add options to the {@link SettingsController} using the static form builder.
     *
     * @return void
     */
    public static function addOptions()
    {
        $builder = SettingsController::getBuilder();
        $plugins = Bootstrap::getPlugins();
        foreach ($plugins as $plugin) {
            $plugin->buildForm($builder);
        }
    }
}
