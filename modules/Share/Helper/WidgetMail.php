<?php

namespace Module\Share\Helper;

use Webwijs\Email;
use Webwijs\Http\Request;

class WidgetMail
{
    /**
     * Toggle the visibility of email sharing.
     *
     * @return html the form which allows a user to share a webpage through an email.
     */
    public function widgetMail()
    {
        $args = array(
            'emailShareOpen'   => '',
            'emailShareNotice' => '',
            'emailShareValue'  => '',
            'emailShareError'  => '',
            'nameShareValue'   => '',
            'nameShareError'   => '',
        );

        $errors  = array();
        $request = new Request();
        if ($request->isPost() && $request->getPost('addthis-email-submit') !== null) {
            $args['emailShareOpen']  = 'open';
            $args['emailShareValue'] = $request->getPost('addthis-email', '');
            $args['nameShareValue']  = $request->getPost('addthis-name', '');

            if (empty($args['nameShareValue'])) {
                $errors['nameShareError'] = __('Geef a.u.b. uw naam op');
            }
            if (empty($args['emailShareValue'])) {
                $errors['emailShareError'] = __('Vul a.u.b. een e-mailadres in');
            } else if (!is_email($args['emailShareValue'])) {
                $errors['emailShareError'] = __('Dit e-mailadres is ongeldig.');
            }

            if (count($errors) === 0) {
                if ($this->sendEmail($args['emailShareValue'], $args['nameShareValue'])) {
                    $args['emailShareNotice'] = '<div class="notify-message success">' . __('De pagina is doorgestuurd naar ') . $args['emailShareValue'] .  '</div>';
                    $args['emailShareValue']  = '';
                    $args['nameShareValue']   = '';
                } else {
                    $args['emailShareNotice'] = '<div class="notify-message error">' . __('De pagina kon niet verstuurd worden') . '</div>';
                }
            }
        }

        return $this->view->partial('partials/sharethis/mail.phtml', array_merge($args, $errors));
    }

    protected function sendEmail($email, $name)
    {
        $mail   = new Email();
        $output = $mail->setTo($email)
                       ->setFrom(array('name' => $name, 'email' => $email))
                       ->setSubject(get_option('theme_share_email_subject'))
                       ->setTemplate('partials/sharethis/mail/content.phtml')
                       ->setTemplateVars(array('name' => $name, 'title' => get_the_title(), 'url' => get_permalink()))
                       ->send();

        return $output;
    }
}
