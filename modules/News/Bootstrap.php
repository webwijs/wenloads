<?php
namespace Module\News;

use Webwijs\Post;
use Webwijs\AbstractBootstrap;
use Webwijs\Module\ModuleInfo;
use Webwijs\Module\ModuleAwareInterface;
use Webwijs\Module\AutoloadableInterface;

use Module\News\Cpt\News as NewsCpt;
use Module\News\Admin\Bootstrap as AdminBootstrap;

// use Webwijs\CSS\MainCompiler as CSSCompiler;

/**
 * The module News bootstrap
 *
 * @author Joris Wagter
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap implements ModuleAwareInterface, AutoloadableInterface
{

    /**
     * The module class
     * @var Module $module the module class
     */
    protected $module;

    /**
     * Adds the News directory to the SPLAutoloader object to retrieve classes within this namespace
     * @return void
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\News' => __DIR__,
                ),
            ),
        );
    }

    /**
     * Sets the module class
     * @param Module $module the module class
     */
    public function setModule(ModuleInfo $module)
    {
        $this->module = $module;
    }

    /**
     * Initializes the News custom post type
     * @return void
     */
    public function _initCpt()
    {
        NewsCpt::register();
		Post::addCustomPostPageId('news', get_option('theme_page_news'));
    }

    /**
     * Initializes the admin bootstrap for the backend.
     * @return void
     */
    public function _initAdmin()
    {
        $adminBootstrap = new AdminBootstrap();
        add_action('admin_menu', array(&$adminBootstrap, 'init'));
    }

}
