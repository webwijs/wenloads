<?php

namespace Module\Search\Helper;

use Webwijs\Dom\HtmlElementBuilder;
use Webwijs\Dom\HtmlElement;
use Webwijs\Dom\TextElement;

/**
 * The SearchSection wraps the search form inside a section.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class SearchSection
{
    /**
     * Displays a search form inside it's own section.
     * Use this helper inside a template or partial as is illustrated in the example below:
     *
     * <code>
     *     $form = $this->searchSection(array(
     *                 'value'  => $this->searchQuery(),
     *                 'method' => 'get'
     *             ));
     * </code>
     *
     * @param array $args (optional) arguments used to build the form.
     * @return ElementInterface object that represents the html element.
     * @see SearchForm
     */
    public function searchSection(array $args = array())
    {
        $form = $this->view->searchForm($args);
        
        // build section
        $builder = new HtmlElementBuilder('div');
        $builder->attributes(array(
                      'id' => 'search',
                      'class' => 'search-container',
                  ))
                ->child(function($builder) use ($form) {
                      // build row
                      return $builder->tag('div')
                                     ->attribute('class', 'row')
                                     ->child(function ($builder) use ($form) {
                                           // build column
                                           return $builder->tag('div')
                                                          ->attribute('class', 'large-9 small-centered columns')
                                                          ->child($form)
                                                          ->build();
                                       })
                                     ->build();
                  });
        
        return $builder->build();
    }
}
