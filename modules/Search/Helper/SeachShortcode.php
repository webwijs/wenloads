<?php

namespace Module\Search\Helper;

/**
 * The SeachShortcode class should be invoked through use of a WordPress shortcode and
 * displays a list of posts for a search query.
 *
 * @author Chris Harris <chris@bwebwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class SeachShortcode
{
    /**
     * A shortcode which returns a list of posts for a search query.
     * This shortcode should be in conjunction with the WordPress editor as following:
     *
     * <code>
     *     [search-results]
     * </code>
     *
     * @see ListSearchResults($args)
     * @see SearchQuery($escaped);
     */
    public function seachShortcode()
    {
        return $this->view->listSearchResults(array('query' => array('s' => $this->view->searchQuery())));
    }
}
