<?php

namespace Module\Search\Helper;

use Webwijs\Http\Request;

/**
 * The SearchQuery returns the search query that was used to find one or more posts.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class SearchQuery
{
    /**
     * Returns the search query from the $_GET superglobal or the {@link get_search_query()} function.
     * Use this helper inside a template or partial as is illustrated in the example below:
     *
     * <code>
     *     $query = $this->searchQuery(false);
     * </code>
     *
     * @param $tidy clean up the search string.
     * @return string returns the found search query.
     */
    public function searchQuery($escaped = true)
    {    
        $request = new Request();
        $search  = $request->getQuery('search');
        if (is_string($search)) {
            $search = apply_filters('get_search_query', $search);
            if ($escaped) {
                $search = esc_attr($search);
            }
        } else {
            $search = get_search_query($escaped);
        }
        
        return $search;
    }
}
