<?php

namespace Module\Facetsearch\Helper;

use Webwijs\FacetSearch\Filter\MultiDate;

use Webwijs\FacetSearch\Filter\MultiFilterInterface;
use Webwijs\Util\Strings;
use Webwijs\View\Helper\FormElement;

/**
 * The FacetedMultiDate will render two date fields which together form a range.
 *
 * @author Chris Harris
 * @version 0.0.1
 */
class FacetedMultiDate extends FormElement
{
    /**
     * Displays two date fields for the given filter.
     *
     * @param FacetedMultiDate $filter the filter for which to display the date fields.
     * @param array $attribs (optional) array of attributes to set for each date field.
     */
    public function facetedMultiDate(MultiDate $filter, array $attribs = array())
    {
        $name  = $filter->getName();
        $dates = $this->normalize($filter->getTimestamps(), $filter->getFormat());

        $output = '';
        foreach ($dates as $date) {
            $output .= sprintf('<input %3$s type="hidden" value="%1$s" name="%2$s[]" />', $date, $name, $this->_renderAttribs($attribs));
        }
        
        return sprintf('<div class="multi-date-container">%s</div>', $output);
    }
    
    /**
     * Normalizes the specified timestamps into date strings of the specified format.
     *
     * @param array $timestamps a collection of timestamps to normalize.
     * @param string $format the format of the date strings.
     * @return array date strings according to the specified format.
     * @throws InvalidArgumentException if the specified format is not a string.
     * @link http://php.net/manual/en/function.date.php date format
     */
    private function normalize(array $timestamps, $format)
    {
        if (!is_string($format)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects the format to be a string; received "%s"',
                __Method__,
                (is_object($format)) ? get_class($format) : gettype($format)
            ));
        }
        
        $dates = array();
        foreach ($timestamps as $timestamp) {
            $dates[] = (is_numeric($timestamp)) ? date($format, $timestamp) : '';
        }
        
        return $dates;
    }
}
