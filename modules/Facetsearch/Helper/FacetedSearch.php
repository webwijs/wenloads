<?php

namespace Module\Facetsearch\Helper;

use Webwijs\View;
use Webwijs\Http\Request;
use Webwijs\Util\Arrays;
use Webwijs\FacetSearch\Factory as FacetedSearchFactory;
use Webwijs\FacetSearch\AbstractFacetSearch as AbstractFacetedSearch;

use Module\Facetsearch\Module;

use LogicException;

class FacetedSearch
{   
    /**
     * Array containing arguments used by this helper.
     *
     * @var array
     */
    private static $args = array();

    /**
     * A object that facilitates in the faceted search.
     *
     * @var AbstractFacetedSearch
     */
    private static $facetedSearch;

    /**
     * Create FacetedSearch helper.
     *
     * @return FacetSearch allows for method chaining.
     */
    public function facetedSearch()
    {
        return $this;
    }

    /**
     * Displays a list of results using faceted search.
     *
     * @param array $args (optional) argument to change what is displayed.
     * @return FacetSearch allows for method chaining.
     */
    public function init($args = null)
    {
        $defaults = array(
            'type' => 'Posts',
            'template' => 'partials/post/faceted-search.phtml',
            'vars' => array(
                'list-item' => 'partials/post/list-item.phtml',
                'not-found' => 'partials/post/not-found.phtml'
            )
        );
        self::$args = Arrays::addAll($defaults, (array) $args);
        
        // create faceted search instance.
        self::$facetedSearch = FacetedSearchFactory::create(self::$args['type']);

        // create wp query from query string.
        self::$facetedSearch->apply(urldecode($_SERVER['QUERY_STRING']));
        
        // perform an asynchronous search.
        $request = new Request();
        if ($request->isXmlHttpRequest()) {
            echo $this->getAjaxResults();
            exit; 
        }
        
        // enqueue necessary scripts.
        add_action('wp_enqueue_scripts', array($this, 'enqueueScripts'));
    }
    
    /**
     * Returns the results from an asynchronous request.
     *
     * @return void
     */
    private function getAjaxResults()
    {
        // find posts using faceted search.
        self::$facetedSearch->queryPosts();

        $result = array();
        $result['status'] = 'success';
        $result['querystring'] = urldecode($_SERVER['QUERY_STRING']);
        // $result['page']   = self::$facetedSearch->getFilter('paginate')->getCurrentPage();
        // $result['pages']  = self::$facetedSearch->getFilter('paginate')->getNumPages();
        $result['items']  = array();
        $result['pagination'] = '';

        if($paginateFilter = self::$facetedSearch->getPaginateFilter()){
            $view = new View();
            $result['page']   = $paginateFilter->getCurrentPage();
            $result['pages']  = $paginateFilter->getNumPages();
            $result['pagination'] = $view->facetedPagination($paginateFilter, array('nextpage' => 'Volgende', 'previouspage' => 'Vorige' ));
        }
        
        $result['query']  = $GLOBALS['wp_query']->request;
        $result['args']  = json_encode(self::$facetedSearch->_args);
        $result['list-item'] = self::$args['vars']['list-item'];
        $result['wpquery'] = self::$facetedSearch->getWpQuery();
        $GLOBALS['wp_query'] = self::$facetedSearch->getWpQuery();
        
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                $result['items'][] = $this->view->partial(self::$args['vars']['list-item']);
            }
        } else {
            $result['items'][] = $this->view->partial(self::$args['vars']['not-found']);
        }
        
        // reset query.
        wp_reset_query();
        
        return json_encode($result);
    }
        
    /**
     * Queue all the necessary scripts (and styles) so that they will be loaded into the webpage.
     *
     * @return void
     */
    public function enqueueScripts()
    {
        $moduleAssetsUri = Module::getAssetsDirectory();
        wp_enqueue_script('address', '//cdnjs.cloudflare.com/ajax/libs/jquery.address/1.6/jquery.address.min.js', array('jquery'));
        wp_enqueue_script('deserialize', $moduleAssetsUri . '/js/jquery.deserialize.js', array('jquery'));
        wp_enqueue_script('faceted-search', $moduleAssetsUri . '/js/jquery.faceted-search.js', array('jquery', 'address', 'deserialize'));   
    }
    
    /**
     * Set a default value for the instantiated FacetSearch object.
     *
     * @param string $name the name associated with the value.
     * @param mixed $value the default value to set.
     * @return FacetSearch allows for method chaining.
     * @throws LogicException if the FacetSearch object has not yet been instantiated.
     * @see AbstractFacetSearch::setDefault($name, $value)
     */
    public function setDefault($name, $value)
    {
        if (self::$facetedSearch === null) {
            throw new LogicException(sprintf(
                '%s: no FacetSearch object initialized, call %s::init() first.',
                __METHOD__,
                __CLASS__
            ));
        }
        
        self::$facetedSearch->setDefault($name, $value);
        return $this;
    }
    
    /**
     * Apply the given parameters to the a new WP_Query object.
     *
     * @param string|array $params a query string or array containing arguments.
     * @param int $page the offset at which to start retrieving results.
     * @throws LogicException if the FacetSearch object has not yet been instantiated.
     * @see AbstractFacetSearch::apply($params, $page)
     */
    public function apply($params, $page = 1)
    {
        if (self::$facetedSearch === null) {
            throw new LogicException(sprintf(
                '%s: no FacetSearch object initialized, call %s::init() first.',
                __METHOD__,
                __CLASS__
            ));
        }
    
        self::$facetedSearch->apply($params, $page);
    }

    /**
     * Displays the output of this helper.
     *
     * @return string the output of this helper.
     */
    public function __tostring()
    {
        // get request url.
        $request = new Request();
        
        // query posts. 
        self::$facetedSearch->queryPosts();
        // var_dump($GLOBALS['wp_query']->request);
        $output = $this->view->partial(self::$args['template'], array_merge((array) self::$args['vars'], array('facetedSearch' => self::$facetedSearch, 'url' => $request->getRequestUrl())));
        // reset query.
        wp_reset_query();
        
        return $output;
    }
}
