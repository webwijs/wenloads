<?php
namespace Module\Facetsearch;

use Webwijs\AbstractBootstrap;
use Webwijs\Post;
use Webwijs\Loader\ClassLoader;
use Webwijs\Module\ModuleInfo;
use Webwijs\Module\ModuleAwareInterface;
use Webwijs\Module\AutoloadableInterface;

use Module\Facetsearch\Module as ModuleContainer;

class Bootstrap extends AbstractBootstrap implements ModuleAwareInterface, AutoloadableInterface
{

    public function getAutoloaderConfig()
    {	
        return array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\Facetsearch' => __DIR__,
                ),
            ),
        );
    }

    /**
     * Add static resources to the class loader.
     */
    protected function _initResourceloaders()
    {    
        ClassLoader::addStaticResources(array(
            'facetsearch'       => 'Module\Facetsearch\FacetSearch',
            'facetsearchfilter' => 'Module\Facetsearch\FacetSearch\Filter'
        ));
    }

    /**
     * Sets the module class
     * @param Module $module the module class
     */
    public function setModule(ModuleInfo $module)
    {
        $this->module = $module;
    }

    public function getModule()
    {
        return $this->module;
    }

    public function _initModuleContainer()
    {
        new ModuleContainer($this->getModule());
    }

}
