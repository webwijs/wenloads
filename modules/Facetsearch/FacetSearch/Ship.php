<?php

namespace Module\Facetsearch\FacetSearch;

use Webwijs\FacetSearch\AbstractFacetSearch as AbstractFacetedSearch;

class Ship extends AbstractFacetedSearch
{
    public function init()
    {
        $this->defaults = array(
            'orderby' => 'post_date',
            'order' => 'desc',
            'posts_per_page' => -1,
            'post_type' => 'ship'
        );
        $this->addFilter('taxonomy', 'ship', array('taxonomy'=>'trip_category', 'first' => 'Select category'));
        $this->addFilter('paginate', 'paginate', array('default' => 1));
    }
}

