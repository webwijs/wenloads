<?php

namespace Module\Facetsearch\FacetSearch;

use Webwijs\FacetSearch\AbstractFacetSearch as AbstractFacetedSearch;

class Reviews extends AbstractFacetedSearch
{
    public function init()
    {
        $this->defaults = array(
            'orderby' => 'post_date',
            'order' => 'desc',
            'posts_per_page' => 4,
            'post_type' => 'reviews'
        );
        $this->addFilter('taxonomy', 'reviews', array('taxonomy'=>'category'));
        $this->addFilter('paginate', 'paginate', array('default' => 1));
    }
}

