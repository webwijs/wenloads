<?php
namespace Module\Usp;

use Webwijs\AbstractBootstrap;
use Webwijs\Module\AutoloadableInterface;
use Webwijs\Shortcode\ViewHelper;

// use Module\Usp\Cpt\Usp as UspCpt;

/**
 * The Usp bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap implements AutoloadableInterface
{
    /**
     * A collection of socials plugins.
     *
     * @var ListInterface
     */
    private static $plugins = null;

    /**
     * Register this module with the Standard PHP Library (SPL).
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\Usp' => __DIR__,
                ),
            ),
        );
    }

    /**
     * Register custom post type with WordPress.
     */
    // protected function _initCpt()
    // {
    //     UspCpt::register();
    // }

    /**
     * Register shortcodes with WordPress.
     */
    protected function _initShortcode()
    {
        $helper = new ViewHelper();
        //add_shortcode('content-block', array($helper, 'blockShortcode'));
    }

    protected function _initWidget()
    {
        register_widget('Module\Usp\Widget\Usp');
    }
}
