<?php

namespace Module\Usp\Widget;

use Webwijs\Http\Request;

/**
 * An abstract widget which will setup all the conditions necessary to display a dropdown with images
 * and show a preview of a selected image.
 *
 * @author Chris Harris
 * @version 0.0.9
 */
abstract class AbstractUspWidget extends \Wp_Widget
{
  public static $icons = array(
              'fa-phone'        => 'Telefoon',
              'fa-clock-o'      => 'Klok',
              'fa-map-marker'   => 'Kaart marker',
              'fa-envelope-o'   => 'Envelop',
              'fa-coffee'       => 'Koffie',
              'fa-handshake-o'  => 'Handenschudden',
              'fa-bar-chart'    => 'Grafiek',
              'fa-briefcase'    => 'Koffer',
              'fa-bicycle'      => 'Fiets');
    /**
     * Construct a new widget.
     *
	 * @param string $id_base (optional) The base ID for the widget, lowercase and unique. If left empty,
	 *                        a portion of the widget's class name will be used Has to be unique.
	 * @param string $name The name for the widget displayed on the configuration page.
	 * @param array  $widget_options (optional) The widget options. See {@see wp_register_sidebar_widget()} for
	 *                               information on accepted arguments. Default empty array.
	 * @param array  $control_options (optional) The widget control options. See {@see wp_register_widget_control()}
	 *                                for information on accepted arguments. Default empty array.
	 * @see Wp_Widget::__construct($id_base, $name, $widget_options, $control_options)
     */
    public function __construct($id_base, $name, $widget_options = array(), $control_options = array())
    {
        parent::__construct($id_base, $name, $widget_options, $control_options);
        add_filter('webwijs_usp_icons', [$this, 'setIcons']);


		// create admin hooks for this widget.
		if(is_admin()) {
		      add_action('wp_ajax_get_attachment_url', array($this, 'getAttachmentUrl'));
			    add_action('admin_enqueue_scripts', array($this, 'enqueueScriptsStyles'));
	       }
    }
    public function setIcons($icons = []){
      self::$icons[] = $icons;
    }
    public function getIcons(){
      return self::$icons;
    }
    /**
     * Converts the given collection of WP_Post ojects into an array that can be used to populate a dropdown form.
     *
     * @param array|\Traversable $posts a collection of posts.
     * @return array an array consisting of (dropdown) options.
     * @throws \InvalidArgumentException if the given argument is not an array or an instance of Traversable.
     */
     protected function asImageOptions($posts)
     {
         if (!is_array($posts) && !($posts instanceof Traversable)) {
             throw new InvalidArgumentException(sprintf(
                 '%s expects an array or Traversable object as argument; received "%d"',
                 __METHOD__,
                 (is_object($posts) ? get_class($posts) : gettype($posts))
             ));
         }

         $options = array();
         foreach ($posts as $post) {
             $options[$post->ID] = $post->post_title;
         }
         return $options;
     }

    protected function asIconOptions($posts)
    {
        if (!is_array($posts) && !($posts instanceof Traversable)) {
            throw new InvalidArgumentException(sprintf(
                '%s expects an array or Traversable object as argument; received "%d"',
                __METHOD__,
                (is_object($posts) ? get_class($posts) : gettype($posts))
            ));
        }


        return $this->getIcons();
    }

    /**
     * Handles an asynchronous HTTP request and finds the image url associated with the given attachment id.
     *
     * The attachment id is retrieved from the POST or GET superglobal depending
     * on the HTTP request method used with the AJAX request.
     *
     * @return void
     * @link http://codex.wordpress.org/AJAX_in_Plugins#Ajax_on_the_Administration_Side
     */
    public function getAttachmentUrl()
    {
        $request = new Request();
        switch ($request->getMethod()) {
            case 'POST':
                $attachmentId = $request->getPost('attachment_id', 0);
                break;
            case 'GET':
            default:
                $attachmentId = $request->getQuery('attachment_id', 0);
                break;
        }
        if ($url = wp_get_attachment_url($attachmentId)) {
            echo esc_url($url);
        }
        exit();
    }

    /**
     * Enqueue necessary scripts into the head of the admin page.
     *
     * @param string $hook identifies a page, which can be used to target a specific admin page.
     * @link http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
     */
    public function enqueueScriptsStyles($hook)
    {
        if ('widgets.php' != $hook) {
            return;
        }
        wp_enqueue_style('font-awesome', 'http://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css');
        wp_enqueue_style('usp-widget-style', get_bloginfo('stylesheet_directory') . '/modules/Usp/assets/css/admin/usp.css');
        wp_enqueue_script('usp-widget', get_bloginfo('stylesheet_directory') . '/modules/Usp/assets/js/admin/jquery.usp-widget.js', array('jquery'), false, true);
    }
}
