<?php

namespace Module\Gallery;

use Webwijs\Module\ModuleInfo as WebwijsModule;

class Module {

    protected static $module;

    public function __construct(WebwijsModule $module = null)
    {
    	if($module){
    		self::setModule($module);	
    	}
    }

    public static function setModule(WebwijsModule $module)
    {
    	static::$module = $module;
    }

    public static function getModule()
    {
    	return static::$module;
    }

    public static function getAssetsDirectory()
    {
        return sprintf('%s/%s', static::$module->getUrl(), 'assets');
    }

}