<?php

namespace Module\Gallery\Helper;

class PostTitle
{
    /**
     * Returns the title for the given post or a default message if no title is available.
     *
     * @param object $post the post object from which the title will be returned.
     * @param array $args optional arguments that can be used to retrieve the title.
     * @return string returns the title, or a default message if no title is available.  
     */
    public function postTitle($post, $args = null)
    {
        if(empty($post)) {
            $post = $GLOBALS['post'];
        }
        
        $defaults = array(
            'message' => '(geen title)',
        );
        $options = array_merge($defaults, (array) $args);
        
	    $title = get_the_title($post);
	    return (!empty($title)) ? $title : $options['message'];
    }
}
