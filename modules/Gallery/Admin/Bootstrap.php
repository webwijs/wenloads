<?php

namespace Module\Gallery\Admin;

use Webwijs\AbstractBootstrap;
use Module\Gallery\Action\Database;
use Module\Gallery\Admin\Metabox\Gallery;

class Bootstrap extends AbstractBootstrap
{
    public function _initDatabase()
    {
        $db = new Database();
        $db->install();
    }

    protected function _initGallery(){
    	// Gallery::register('gallery');
    	Gallery::register(array('page', 'news', 'aanbod', 'gallery'));
    }
}
