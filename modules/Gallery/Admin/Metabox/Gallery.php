<?php

namespace Module\Gallery\Admin\Metabox;

use Webwijs\View;
use Module\Gallery\Admin\Metabox;
use Module\Gallery\Post;
use Module\Gallery\Module;

class Gallery extends Metabox
{
    static public $id = 'webwijs-gallery';
    static public $title = 'Galerij';
    static public $context = 'normal';
    static public $priority = 'default';

    public $meta_key = 'gallery';
    
    /**
     * 
     */
    public function init()
    { 
        add_action('admin_enqueue_scripts', array($this, 'enqueueScripts'));
    }
    
	/**
	 * Loads all the necessary scripts which allows this metabox to behave and display normal.
	 *
	 * @param string $hook name of the currently active page.
	 */
	public function enqueueScripts($hook) {
        $moduleAssetsUri = Module::getAssetsDirectory();
        
		// only load on select pages
		if (in_array($hook, array('post-new.php', 'post.php', 'media-upload-popup'))) {
		    wp_enqueue_media();
		    
            wp_enqueue_script('webwijs-gallery', $moduleAssetsUri . '/js/jquery.webwijs.gallery.js', array('jquery'));
            wp_enqueue_style('webwijs-gallery-style', $moduleAssetsUri . '/css/admin.css');		    
		}
	}

    public function display($post)
    {    
        if (!did_action('wp_enqueue_media')) {
            wp_enqueue_media(array('post' => $post->ID ));
        }
	    $upload_iframe_src = esc_url(get_upload_iframe_src('image', $post->ID));
        $view = new View();
    ?>
        <div class="content std">
            <a class="button add-gallery-image" href="<?php echo $upload_iframe_src ?>" data-uploader_title="Galerij afbeelding invoegen" data-uploader_button_text="Afbeelding invoegen">Afbeelding toevoegen</a>
        </div>
    <?php
	    $posts = $this->getPosts();
	    if(is_array($posts) || ($posts instanceof Traversable)): ?>
        <table class="webwijs-gallery-table">
            <tbody>
                <?php echo $view->rowBuilder($posts, array('meta_key' => $this->meta_key)) ?>
            </tbody>
        </table>
        <?php endif ?>
        <script type="text/javascript">
            var gallery = jQuery('#<?php echo self::$id ?>').gallery({ 
                settings: { 
                    ajaxurl: "<?php echo admin_url('admin-ajax.php') ?>",
                    key: "<?php echo $this->meta_key ?>",
                } 
            });
        </script>
    <?php
    }
    
    public function save($postId)
    {
        $attachments = array();
        if (!empty($_POST['attachments'][$this->meta_key])) {
            foreach ((array) $_POST['attachments'][$this->meta_key] as $attachmentId => $attachment) {
                if(isset($attachment['sort_order'])) {
                    $attachments[$attachmentId] = $attachment['sort_order'];
                }
            }
        }

        Post::updateAttachment($postId, $attachments, $this->meta_key);
    }
    
    /**
     * Gets all attachments for the post that's currently been editing.
     *
     * @return array returns an array containing all the attachments associated with this post.
     */
    public function getPosts()
    {
        $results = array();

        global $post, $wpdb;

        $attachments = array();
        $results = $this->getAttachments($this->meta_key, $post);
        foreach($results as $result) {
            $attachments[$result->attachment_id] = $result->sort_order;
        }

        $images = null;
        if(!empty($attachments)) {
            $images = get_posts(array(
                'include' => implode(', ', array_keys($attachments)),
                'post_type' => 'attachment'
            ));

            foreach($images as $image) {
                if(isset($attachments[$image->ID])) {
                   $image->sort_order = $attachments[$image->ID];
                }
            }
            
            // sorts the array with posts objects based on their 'sort_order'.
            usort($images, array($this, 'comparator'));
        }
        return $images;
    }
    
    protected function getAttachments($key = null, $post = null)
    {
        global $wpdb;
        if (empty($post)) {
            $post = $GLOBALS['post'];
        }
        $keyCondition = '';
        if (!empty($key)) {
            $keyCondition = $wpdb->prepare(' AND relation_key = %s', $key);
        }

        $subQuery = <<<SQL
            SELECT post_id, attachment_id, relation_key, sort_order
            FROM {$wpdb->prefix}gallery
            WHERE post_id = %d {$keyCondition}
            ORDER BY sort_order ASC
SQL;

        $subQuery = $wpdb->prepare($subQuery, $post->ID, $key);
        return $wpdb->get_results($subQuery);    
    }
    
    /**
     * Compares two post objects and is used to sort all posts based on their 'sort_order'.
     *
     * @param mixed $a the first object that will compared with the second object.
     * @param mixed $b the second object that will compared with the first object.
     * @return int returns a numeric value which helps determine where the post should be placed inside the array.
     */
    private function comparator($a, $b)
    {
        $sort = 0;
        if(isset($a->sort_order) && isset($b->sort_order)) {
            if($a->sort_order == $b->sort_order) {
                return $sort;
            }
            $sort = ($a->sort_order > $b->sort_order) ? 1 : -1;
        }
        return $sort;
    }
}
