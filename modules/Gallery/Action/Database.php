<?php

namespace Module\Gallery\Action;

class Database
{
    public static function install()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . 'gallery';
        if($wpdb->get_var("SHOW TABLES LIKE '". $table_name ."'") != $table_name) {
            $sql = "CREATE TABLE IF NOT EXISTS ". $table_name ." (
                post_id bigint(20) UNSIGNED NOT NULL,
                attachment_id bigint(20) UNSIGNED NOT NULL,
                relation_key varchar(255) NOT NULL,
                sort_order int(11) NOT NULL,
                PRIMARY KEY (post_id, attachment_id),
                FOREIGN KEY (post_id) REFERENCES ". $wpdb->posts ."(ID),
                FOREIGN KEY (attachment_id) REFERENCES ". $wpdb->posts ."(ID)
            );";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }
    }
}
