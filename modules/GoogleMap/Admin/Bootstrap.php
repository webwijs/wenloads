<?php

namespace Module\GoogleMap\Admin;

use Webwijs\AbstractBootstrap;
use Webwijs\Admin\Metabox\PageLayout;
use Webwijs\Admin\Metabox\Excerpt;
use Webwijs\Admin\Metabox\Multibox;
use Theme\Admin\Controller\SettingsController;
use Theme\Admin\Controller\DeveloperSettingsController;
use Theme\Admin\Controller\SidebarsController;

use Module\GoogleMap\Bootstrap as ModuleBootstrap;
use Module\GoogleMap\Admin\Metabox\Data;

/**
 * The admin bootstrap of the GoogleMaps module
 *
 * @author Joren de Graaf <jorendegraaf@gmail.com>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap
{
    protected function _init(){
      add_action('admin_head', array(&$this, 'menuLayout'));
    }

    protected function _initSettings(){
		$builder = SettingsController::getBuilder();
		$builder->group('company')
			->add('theme_company_address_coords', 'address', array('label' => 'Google Maps:<br/> Standaard kaart positie zoeken'))
			->add('theme_googlemaps_zoom', 'select', array(
			      'label' => 'Google Maps: Zoom niveau*<br/> <sub>* hoe hoger het nummer is, hoe verder de kaart inzoomt</sub>',
			      'options' => $this->generateZoomlevels(26)));


	 	$developer = DeveloperSettingsController::getBuilder();
		$developer->group('googlemaps', 'Google Maps')
			->add('theme_googlemaps_key', 'text', array('label' => 'Google Maps API key'))
			->add('theme_googlemaps_hide_default_marker', 'checkbox', array('label' => 'Verberg standaard marker'))
			->add('theme_googlemaps_hide_default_popup', 'checkbox', array('label' =>  'Verberg standaard pop-up'))
			->add('theme_googlemaps_hide_menu', 'checkbox', array('label' => 'Verberg menu'));
    }

    protected function _initAdminScripts(){
      add_action('admin_enqueue_scripts', [&$this, 'adminJsCss']);
    }

    public function adminJsCss(){
      $module = ModuleBootstrap::getModule();
      wp_enqueue_script('gmaps-ext', "//maps.google.com/maps/api/js?key=AIzaSyA819HrIwc3cjPntSqkUEYsAPzLRahLytA");
      wp_enqueue_script('gmaps-wrapper', $module->getUrl(). '/assets/js/gmaps-wrapper.js', array('gmaps-ext'));
      wp_enqueue_script('gmaps-admin', $module->getUrl(). '/assets/js/gmaps-admin.js', array('gmaps-wrapper'));
      wp_enqueue_style('gmaps-css', $module->getUrl(). '/assets/css/admin/gmaps-admin.css');
    }

    public function generateZoomlevels($howMany = 31){
      $options = [];
      for($i = 1; $i < $howMany; $i++){
          $options[] = $i;
      }
      return $options;
    }
    protected function _initAdminLayout(){
      global $menu, $submenu;
      if (get_option('theme_hide_googlemaps')) {
          foreach ($submenu['edit.php'] as $key => $item) {
              if (strpos($item[2], 'post_type=googlemap')) {
                  unset($submenu['edit.php'][$key]);
              }
          }
      }
    }

    protected function _initMetaboxes()
    {
    	$dataMetabox = new Data();
      $dataMetabox->register();
    }

    public static function menuLayout()
    {?>
        <style type="text/css">
            <?php if (get_option('theme_hide_googlemaps') == 1): ?>
            #menu-posts-googlemap { display: none }
            <?php endif; ?>
        </style>
        <?php
    }
}
