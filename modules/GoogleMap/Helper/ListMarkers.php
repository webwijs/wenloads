<?php

namespace Module\GoogleMap\Helper;
use Module\GoogleMap\Bootstrap;
use Webwijs\Post;
use Webwijs\Util\Arrays;

/**
 * Helper for displaying a list of employees
 *
 * @author Joren de Graaf
 * @version 1.0.0
 */
class ListMarkers
{

	/**
	 * Function to list employees.
	 * Arguments can be used to filter the employees or display the employees in a certain template.
	 * @param  array $args array containing arguments
	 * @return string $output the html output of the retrieved employees in a certain template, or null if no employees are found
	 */
    public function listMarkers($args = null)
    {
			$module = Bootstrap::getModule();
      $defaults = array(
          'queryArgs' => array(
              'post_type' 	=> 'googlemap',
              'nopaging' 		=> true,
              'orderby' 		=> 'menu_order',
              'order' 			=> 'asc'
          ),
          'template' 	=> 'partials/googlemap/markers.phtml',
					'show_default_marker' => get_option('theme_googlemaps_hide_default_marker', true),
					'vars'			=>	['marker' => $module->getUrl() . '/assets/images/gmap_marker.png']
      );
      $args = Arrays::addAll($defaults, (array) $args);
			$default_marker = array('address' => get_option('theme_company_address', true),
															'marker'  => $module->getUrl() . '/assets/images/gmap_marker.png',
															'coords'	=> get_option('theme_company_address_coords', true),
															'zoom'		=> get_option('theme_googlemaps_zoom', true),
															'hide_popup'	=> get_option('theme_googlemaps_hide_default_popup', true),
															'hide_marker'	=> get_option('theme_googlemaps_hide_default_marker', true)
														);
      $output = '';
      query_posts($args['queryArgs']);
      if (have_posts() && !$args['show_default_marker']) {
          $output = $this->view->partial($args['template'], $args['vars']);
      }
			else{
				$output = $this->view->partial('partials/googlemap/default-marker.phtml', $default_marker);
			}
      wp_reset_query();

      return $output;
    }

}
