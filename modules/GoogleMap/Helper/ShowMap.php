<?php
namespace Module\GoogleMap\Helper;
use Module\GoogleMap\Bootstrap;
use Webwijs\Post;
use Webwijs\Util\Arrays;

class ShowMap
{
  public function showMap($args = null){

    $module = Bootstrap::getModule();
    $defaults = [
      'queryArgs' => [
        'post_type' => 'googlemap',
        'nopaging'  => true,
        'orderby'   => 'menu_order',
        'order'     => 'asc'],
      'template'    => 'partials/googlemap/map.phtml',
      'vars'        => ['styles' => ['height' => '400px'],
                        'marker' => $module->getUrl() . '/assets/images/gmap_marker.png']];
    $args = Arrays::addAll($defaults, (array) $args);
    // Generate the map
    $output = '';
    $output = $this->view->partial($args['template'], $args['vars']);
    return $output;
  }
}
?>
