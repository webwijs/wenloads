<?php
namespace Module\GoogleMap\Helper;
/**
 * Generates a route/directions link for Google Maps 
 * @author Joren de Graaf <jorendegraaf@gmail.com>
 * @version 0.5
 */
class GenerateDirections {
	public function generateDirections() {
		$params = [
			'api'	=> '1',
			'destination' => get_option('theme_company_address_coords')
		];
		$url = "https://www.google.com/maps/dir/?";
		$args = http_build_query($params, '', '&amp;');
		return $url . $args;
	}
}
?>
