<?php
namespace Module\GoogleMap\Helper;

use Webwijs\Post;
use Webwijs\Util\Arrays;
/**
 * Helper for data formatting lat/long values
 * @author Joren de Graaf <jorendegraaf@gmail.com>
 * @todo implement something like this? https://colinyeoh.wordpress.com/2013/02/12/simple-php-function-to-get-coordinates-from-address-through-google-services/
 */
class LatLong
{
  // Provides the lat/long data as an object
  public function latLong($data){
    $defaults = array('lat' => 0, 'long' => 0);
    $data = explode(',', $data);
    $data = array('lat' => $data[0], 'long' => $data[1]);
    $data = array_merge($defaults, $data);
    $data = (object) $data;
    return $data;

  }
}
