<?php
namespace Module\GoogleMap\Helper;
/**
 * Formats a address string into separate HTML elements which you can easily style
 * @author Joren de Graaf <jorendegraaf@gmail.com>
 * @version 0.5
 */
class FormatAddress {
	public function formatAddress($address, $args = []) {
		// Default arguments
		$defaults = [
			'sep' => ', ',
			'element' => 'span',
			'class' => 'address-part'
		];
		$args = wp_parse_args( $args, $defaults );
		// Explode the address string into its separate parts
		$addressPieces = explode($args['sep'], $address);
		$result = [];
		// Loop through the parts
		for($i = 0; $i < count($addressPieces); $i++) {
			// Build the result as a new array
			$result[] = "<{$args['element']} class=\"{$args['class']}-$i\">{$addressPieces[$i]}</{$args['element']}>";
		}
		// Return the final array
		return implode($result);
	}
}
?>
