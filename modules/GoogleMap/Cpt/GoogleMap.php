<?php

namespace Module\GoogleMap\Cpt;

use Webwijs\Cpt;

/**
 * The Employee custom post type
 *
 * @author Leo Flapper
 * @version 1.0.0
 */
class GoogleMap extends Cpt
{
    /**
     * The post type name
     * @var string $type the post type name
     */
    public $type = 'googlemap';

    /**
     * Sets the labels and settings for the custom post type
     * @return void
     */
    public function init()
    {
        $this->labels = array(
            'name'                  => __('Google Maps', 'googlemap'),
            'singular_name'         => __('Marker', 'googlemap'),
            'add_new'               => __('Nieuwe marker', 'googlemap'),
            'add_new_item'          => __('Nieuwe marker', 'googlemap'),
            'edit_item'             => __('Marker bewerken', 'googlemap'),
            'new_item'              => __('Nieuwe marker', 'googlemap'),
            'view_item'             => __('Marker bekijken', 'googlemap'),
            'search_items'          => __('Marker zoeken', 'googlemap'),
            'not_found'             => __('Geen markers gevonden', 'googlemap'),
            'not_found_in_trash'    => __('Geen markers gevonden', 'googlemap'),
            'menu_name'             => __('Google Maps', 'googlemap')
        );

        $this->settings = array(
            'rewrite'       => false,
            'hierarchical'  => false,
            'public'        => false,
            'show_ui'       => true,
            'supports'      => array('title'),
            'menu_icon'     => 'dashicons-location'
        );
    }

    /**
     * Registers the custom post type
     * @param  array $options the options for registering the custom post type
     * @return void
     */
    public static function register($options = null)
    {
        new self($options);
    }

    /**
     * Default query function for retrieving post of the custom post type
     * @param  array $args array of arguments for retrieving the posts
     * @return WP_Query $posts WP Query object containing the retrieved posts
     */
    public static function queryPosts($args = null)
    {
        $defaults = array(
            'post_type' => $this->getType(),
            'orderby' => 'menu_order',
            'order' => 'ASC',
        );
        $args = array_merge( $defaults, (array) $args);

        return query_posts($args);
    }

    /**
     * Returns the post type name
     * @return string $type the post type name
     */
    public function getType()
    {
        return $this->type;
    }
}
