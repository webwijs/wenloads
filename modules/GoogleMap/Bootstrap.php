<?php
namespace Module\GoogleMap;

use Webwijs\AbstractBootstrap;
use Webwijs\Module\ModuleInfo;
use Webwijs\Module\ModuleAwareInterface;
use Webwijs\Module\AutoloadableInterface;

use Module\GoogleMap\Cpt\GoogleMap as GoogleMapCpt;
use Module\GoogleMap\Admin\Bootstrap as AdminBootstrap;

use Webwijs\CSS\MainCompiler as CSSCompiler;

/**
 * The module GoogleMap bootstrap
 *
 * @author Joren de Graaf <jorendegraaf@gmail.com>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap implements ModuleAwareInterface, AutoloadableInterface
{

    /**
     * The module class
     * @var Module $module the module class
     */
     private static $module = null;

    /**
     * Adds the Employee directory to the SPLAutoloader object to retrieve classes within this namespace
     * @return void
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\GoogleMap' => __DIR__,
                ),
            ),
        );
    }

    public function setModule(ModuleInfo $module)
    {
        self::$module = $module;
    }

    /**
     * Returns the {@link ModuleInfo} instance for this module.
     *
     * @return ModuleInfo object containing information about this module.
     */
    public static function getModule()
    {
        return self::$module;
    }
    /**
     * Initializes the Employee custom post type
     * @return void
     */
    public function _initCpt()
    {
        if(get_option('theme_googlemaps_hide_menu') !== '1'){
          GoogleMapCpt::register();
        }
        // Enqueue scripts and styles
        add_action('wp_enqueue_scripts', array($this, 'loadScripts'));
        add_action('wp_enqueue_scripts', array($this, 'loadStyles'));

    }
    /*
     * Load internal and external scripts for the map to work.
     */
    public function loadScripts(){
      $module = Bootstrap::getModule();
	  $key = (get_option('theme_googlemaps_key')? get_option('theme_googlemaps_key'): 'AIzaSyA819HrIwc3cjPntSqkUEYsAPzLRahLytA');
      wp_enqueue_script('gmaps-ext', "//maps.google.com/maps/api/js?key=$key", array('jquery'), false, true);
      wp_enqueue_script('gmaps-wrap', $module->getUrl() . '/assets/js/gmaps-wrapper.js', ['gmaps-ext', 'jquery']);
      wp_enqueue_script('gmaps-lib', $module->getUrl() . '/assets/js/gmaps-library.js', ['gmaps-wrap', 'jquery']);
      wp_enqueue_script('gmaps-init', $module->getUrl() . '/assets/js/gmaps-init.js', ['gmaps-lib', 'jquery']);
    }
    /**
     * Load some default styles
     */
    public function loadStyles(){
      $module = Bootstrap::getModule();
      wp_enqueue_style('gmaps', $module->getUrl() . '/assets/css/gmaps.css');
    }
    /**
     * Initializes the admin bootstrap for the backend.
     * @return void
     */
    public function _initAdmin()
    {
        $adminBootstrap = new AdminBootstrap();
        add_action('admin_menu', array(&$adminBootstrap, 'init'));
    }

}
