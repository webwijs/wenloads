(function( $ ){
    //console.log('init');
    $.fn.maps = function ( options ) {
        return this.each(function() {
            // Create variables
            var $element = $(this);
            var markers = [];
            var styles = [
				{
					"featureType":
					"administrative",
					"elementType":"all",
					"stylers": [
						{"saturation":"-100"}
				]},
				{
					"featureType":
					"administrative.province",
					"elementType":"all",
					"stylers":
						[{"visibility":"off"}]
				},
				{
					"featureType":"landscape",
					"elementType":"all",
					"stylers": [
						{"saturation":-100},
						{"lightness":65},
						{"visibility":"on"}]
				},
				{
					"featureType": "poi",
					"elementType":"all",
					"stylers": [
						{"saturation":-100},
						{"lightness":"50"},
						{"visibility":"simplified"}]
				},
				{
					"featureType":"road",
					"elementType":"all",
					"stylers": [{"saturation":"-100"}]
				},
				{
					"featureType":"road.highway",
					"elementType":"all",
					"stylers":[
						{"visibility":"simplified"} ]
				},
				{
					"featureType":"road.arterial",
					"elementType":"all",
					"stylers":[{"lightness":"30"}]
				},
				{
					"featureType":"road.local",
					"elementType":"all",
					"stylers":[
						{"lightness":"40"}]
					},
				{
					"featureType":"transit",
					"elementType":"all",
					"stylers":[
						{"saturation":-100},
						{"visibility":"simplified"}]
				},
				{
					"featureType":"water",
					"elementType":"geometry",
					"stylers":[
						{"hue":"#ffff00"},
						{"lightness":-25},
						{"saturation":-97}]
				},
				{
					"featureType":"water",
					"elementType":"labels",
					"stylers": [
						{"lightness":-25},
						{"saturation":-100}]
					}
				];
            // Map initializing variables
            var $target             = $($element.data('target')),
                $markers            = $('.marker', $element),
                lat                 = $element.data('lat') || 0,
                lng                 = $element.data('lng') || 0,
                zoom                = $element.data('zoom') || 15,
                scrollwheel         = $element.data('scrollwheel'),
                zoomControl         = $element.data('zoom-control') || false,
                mapTypeControl      = $element.data('map-type-control'),
                scaleControl        = $element.data('scale-control'),
                streetViewControl   = $element.data('street-view-control'),
                rotateControl       = $element.data('rotate-control'),
                fullscreenControl   = $element.data('fullscreen-control'),
                useStyles           = $element.data('use-styles') || styles;

            var map = new GMaps({
                el: $element.data('target'),
                lat: lat,
                lng: lng,
                zoom: zoom,
                scrollwheel: scrollwheel,
                zoomControl: zoomControl,
                mapTypeControl: mapTypeControl,
                scaleControl: scaleControl,
                streetViewControl: streetViewControl,
                rotateControl: rotateControl,
                fullscreenControl: fullscreenControl,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
				styles : useStyles
            });
            var latlng;
            GMaps.geocode({
                address: $element.data('address'),
                callback: function(results, status) {
                  if (status == 'OK') {
                    latlng = results[0].geometry.location;
                    map.setCenter(latlng.lat(), latlng.lng());
                  }
            }});
            window.gmap = map;
            //console.log('init map');
            $markers.each(function(i, e) {
                var $e    = $(e);
                var lat   = $e.data('lat'),
                    lng   = $e.data('lng'),
                    title = $e.data('title'),
                    image = $e.data('image'),
                    icon  = $e.data('icon') || 'http://maps.google.com/mapfiles/marker_green.png';
                if(!$('.infowindow-container', $e).html()){
                    var marker = {
                        lat: lat,
                        lng: lng,
                        title: title,
                        icon: icon
                    }
                }
                else{
                    var marker = {
                        lat: lat,
                        lng: lng,
                        title: title,
                        icon: icon,
                        infoWindow: {
                            content: $('.infowindow-container', $e).html()
                        }
                    }

                }

                window.marker = map.addMarker(marker);
                // Trigger the info window based on an external toggle, from outside the map
                $('#'+$e.data('toggle')).click(function(e){
                    e.preventDefault();
                    google.maps.event.trigger(window.gmap.markers[i], 'click');
                    return false;
                 });
                google.maps.event.addDomListener(window, "resize", function() {
                   window.gmap.setCenter(marker.lat, marker.lng);
                });

            });
        });
    };
}( jQuery ));
