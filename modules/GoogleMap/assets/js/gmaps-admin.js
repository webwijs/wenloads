jQuery( document ).ready( function( $ ) {
  // If the input loses focus then hide the results
  $('input.geocoding').on('blur', function(e){ $('.geocoding-results').removeClass('showresults'); });
  $('.geocoding').bind('input', debounce(function(e){
    // Set variables
    var $elem = $(this),
        target = $elem.data('target'),
        geocode_results = $('.geocoding-results');
    message('Bezig met zoeken', 'orange');

    // Use geocode to search for an address
    GMaps.geocode({
      address: $elem.val(), // The address value
      callback: function(results, status){
        if(status == "OK"){
          var output = "";
          geocode_results.removeClass('showresults');
          $.each(results, function(index, value){
            var latlng =  value.geometry.location;
            output += "<li data-lat="+latlng.lat()+" data-lng="+latlng.lng()+">"+value.formatted_address+"</li>";
          });
          message('Resultaten gevonden', 'green');

          geocode_results.html("<ul>"+output+"</ul>").on('mousedown click', 'li', function(e){
            var li = $(this),
                lat = li.data('lat'),
                lng = li.data('lng');
                console.log(lat, lng);
                $('input[name="'+target+'"]').val(lat+','+lng);
                geocode_results.removeClass('showresults');
                message('Coordinaten toegevoegd, vergeet niet op te slaan', 'green');

          });
          geocode_results.addClass("showresults");

        }
        else if(status == "ZERO_RESULTS"){
          geocode_results.removeClass('showresults');
          message('Er zijn geen resultaten gevonden', 'red');
        }
        else{
          geocode_results.removeClass('showresults');
          message('Er is een fout opgetreden: '+status, 'red');
        }
      }
    });
  }, 1000));
});
function message(message, status){
  var $elem = jQuery('.address-container .status ');
  $elem.attr({'class': 'status'});
  $elem.addClass(status);
  jQuery('p', $elem).text(message);
  setTimeout(function(){
    $elem.attr({'class': 'status'});
  }, 5000);
}
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};
