<?php
/*
 * Template Name: Contact
 * Layouts: contact
 */
 the_post();
	echo $this->partial('partials/parts/extra-title.phtml'); // optional title meta box
	echo $this->partial('partials/parts/subnav.phtml'); // optional subnavigation widget
?>
<section class="row singular template-<?php echo get_post_type() ?>" >
	<div class="large-6 small-12 columns col-left">
		<div class="singular-content">
			<figure class="entry-header">
				<?php
					$image = get_the_post_thumbnail_url(get_the_ID(), 'news-article-header');
					if(!empty($image)):
						?>
						<img src="<?php echo $image;?>"/>
					<?php endif;?>
				</figure>
			<?php echo $this->sidebarArea('contact-left') ?>
			<?php echo $this->phone(get_option('theme_company_phone'), 'normal', ['class' => 'phone big']);?>
			<div class="mail-container">
				<a href="mail:<?php echo get_option('theme_company_email');?>" class="mail big"><?php echo get_option('theme_company_email');?></a>
			</div>
			<div class="location-container">
				<?php echo $this->sidebarArea('visitus');?>
				<span  class="location big"><?php echo get_option('theme_company_address'); ?></span>
				<a href="<?php echo $this->generateDirections();?>" class="route-link" target="_blank">Routebeschrijving</a>
			</div>
			<dl class="company-registry">
				<dt>KVK</dt>
				<dd><?php echo get_option('theme_company_commerce_number');?></dd>
				<dt>BTW</dt>
				<dd><?php echo get_option('theme_company_tax_number');?></dd>
			</dl>

		</div>
	</div>
	<div class="large-6 small-12 columns col-content">
		<?php echo $this->partial('partials/page/singular.phtml') ?>
	</div>
</section>
<section class="google-map">
	<?php echo $this->showMap();?>
	<div class="info-window">
		<div class="location-container">
			<h2><?php echo bloginfo('site_title');?></h2>
			<br/>
			<span  class="location big"><?php echo $this->formatAddress(get_option('theme_company_address')); ?></span>
			<a href="<?php echo $this->generateDirections();?>" class="route-link" target="_blank">Routebeschrijving</a>
		</div>

	</div>
</section>
<?php
	echo $this->partial('partials/parts/page-bottom.phtml'); // breadcrumbs and social buttons
?>
