<?php
/**
 * Template Name: Homepage
 * Layouts: home
 */
?>
<?php the_post(); ?>

<section class="homeusps">
    <div class="row align-center">
        <div class="small-12 column">
            <?php echo $this->partial('partials/page/singular.phtml') ?>
        </div>
    </div>
    <div class="row align-center">
        <div class="small-12 column">
            <div class="row">
                <?php echo $this->sidebarArea('content-usps') ?>
            </div>
        </div>
    </div>
</section>

<?php echo $this->featuredMeubelsoorten(); ?>

<section class="featured">
    <?php echo $this->sidebarArea('content-featured') ?>
</section>

<section class="homeproducts">
    <div class="row">
        <div class="small-12 column">
            <center>
                <h2 class="sectiontitle small"><?php _e('Greep uit ons assortiment'); ?></h2>
            </center>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <?php echo do_shortcode( '[featured_products per_page="4" columns="4"]' ); ?>
        </div>
    </div>
</section>